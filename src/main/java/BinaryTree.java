public class BinaryTree {

    public static class Node {
        public int data;
        public Node left;
        public Node right;
        public Node parent;
        public Node(int value) {
            data = value;
        }

        public void setLeft(Node node) {
            this.left = node;
            if (node != null)
                node.parent = this;
        }

        public void setRight(Node node) {
            this.right = node;
            if (node != null)
                node.parent = this;
        }
    }


    public static Node minimum(Node node) {

        while (node.left != null)
            node = node.left;
        return node;
    }


    public static Node maximum(Node node) {

        while (node.right != null)
            node = node.right;
        return node;
    }


    public static Node successor(Node node) {

        if (node == null)
            return null;

        if (node.right != null)
            return minimum(node.right);

        Node parent = node.parent;
        while (parent != null && parent.right == node) {
            node = parent;
            parent = parent.parent;
        }

        return parent;
    }

    public static Node predecessor(Node node) {

        if (node == null)
            return null;

        if (node.left != null)
            return maximum(node.left);

        Node parent = node.parent;
        while (parent != null && parent.left == node) {
            node = parent;
            parent = parent.left;
        }

        return parent;
    }



    public static void inorderPrint(Node node) {

        if (node == null)
            return;

        inorderPrint(node.left);
        System.out.print(node.data + " ");
        inorderPrint(node.right);
    }


    public static void main(String[] args) {

        Node root = new Node(11);
        root.setLeft(new Node(2));
        root.setRight(new Node(29));
        root.left.setLeft(new Node(1));
        root.left.setRight(new Node(7));
        root.right.setLeft(new Node (15));
        root.right.setRight(new Node(40));
        root.right.right.setLeft(new Node(35));

        inorderPrint(root);
        System.out.println();

        System.out.println(minimum(root.right).data);
        System.out.println(maximum(root.left).data);

        System.out.println(successor(root.right.right.left).data);
        System.out.println(successor(root.right).data);

        System.out.println(predecessor(root.right.right.left).data);
        System.out.println(predecessor(root.right).data);

    }
}
