import java.util.Set;
import java.util.TreeSet;

public class CombinationsWithRepetitions {

    private static void printState(int[] state) {
        for (int i = 0; i < state.length; i++)
            System.out.print(state[i] + " ");
        System.out.println();
    }

    public static void combinations(int[] state, boolean[] reservations, int p, int N, int K) {
        if (p == K) {
            printState(state);
            return;
        }

        for (int i = 1; i <= N; i++ )
            if (! reservations[i]) {
                state[p] = i;
                reservations[i] = true;
                combinations(state, reservations, p + 1, N, K);
                reservations[i] = false;
            }
    }

    public static void printRecursive(int N, int K) {
        boolean[] reservations = new boolean[N];
        int[] state = new int[K];

        combinations(state, reservations, 0, N, K);
    }


    public static void printNonRecursive(int N, int K) {
        boolean[] reservations = new boolean[N + 1];
        int[] state = new int[K];

        int p = 0;
        while (p >= 0) {
            if (p == K) {
                printState(state);
                p--;
                continue;
            }

            reservations[state[p]] = false;

            int c;
            // find next value for current position
            for (c = state[p] + 1; c <= N; c++)
                if (!reservations[c])
                    break;

            //c = next value
            if (c > N) {
                p--;
                continue;
            }

            state[p++] = c;
            reservations[c] = true;

            if (p < K)
                state[p] = 0;
        }
    }

    public static void main(String[] args) {
        CombinationsWithRepetitions.printRecursive(5, 3);
    }
}
