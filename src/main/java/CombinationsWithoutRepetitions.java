public class CombinationsWithoutRepetitions {

    private static void printState(int[] state) {
        for (int i = 0; i < state.length; i++)
            System.out.print(state[i] + " ");
        System.out.println();
    }

    public static void combinations(int[] state, int p, int s, int N, int K) {
        if (p == K) {
            printState(state);
            return;
        }

        for (int i = s; i <= N; i++) {
            state[p] = i;
            combinations(state, p + 1, i + 1, N, K);
        }
    }

    public static void printRecursive(int N, int K) {
        int[] state = new int[K];

        combinations(state, 0, 1, N, K);
    }


    public static void printNonRecursive(int N, int K) {
        int[] state = new int[K];

        int p = 0;
        while (p >= 0) {
            if (p == K) {
                printState(state);
                p--;
                continue;
            }

            int c;
            // find next value for current position
            for (c = state[p] + 1; c <= N; c++)
                if (c <= N)
                    break;

            //c = next value
            if (c > N) {
                p--;
                continue;
            }

            state[p++] = c;

            if (p < K)
                state[p] = c;
        }
    }

    public static void main(String[] args) {
        CombinationsWithoutRepetitions.printNonRecursive(5, 3);
    }
}
