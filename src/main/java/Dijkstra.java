import java.util.*;
import java.util.PriorityQueue;

public class Dijkstra {

    private static class Tag {
        int node;
        int cost;
    }

    private static Tag[] dijkstra(int s, int n, List<List<Tag>> incedence) {

        boolean[] complete = new boolean[n];

        final Tag[] costs = new Tag[n];
        for (int i = 0; i < n; i++) {
            Tag tag = new Tag();
            tag.cost = Integer.MAX_VALUE;
            costs[i] = tag;
        }
        costs[s].cost = 0;
        costs[s].node = -1;

        PriorityQueue<Integer> queue = new PriorityQueue<>(n, (o1, o2) -> {
            return Integer.compare(costs[o1].cost, costs[o2].cost);
        });
        queue.add(s);

        while (queue.size() > 0) {
            int x = queue.poll();
            if (complete[x])
                continue;

            List<Tag> edges = incedence.get(x);
            for (Tag e: edges)
                if (!complete[e.node]) {
                    int c = costs[x].cost + e.cost;
                    if (costs[e.node].cost > c) {
                        costs[e.node].cost = c;
                        costs[e.node].node = x;
                        queue.add(e.node);
                    }
                }


            complete[x] = true;
        }

        return costs;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner("5 7 " +
                "0 1 5 " +
                "0 4 3 " +
                "2 3 8 " +
                "3 4 9 " +
                "4 1 3 " +
                "4 2 7 " +
                "2 1 12 "
        );

        int n = scanner.nextInt();
        int k = scanner.nextInt();
        List<List<Tag>> incedence = new ArrayList<>();
        for (int i = 0; i < n; i++)
            incedence.add(new ArrayList<>());

        for (int i = 0; i < k; i++) {
            int s = scanner.nextInt();
            Tag edge = new Tag();

            edge.node = scanner.nextInt();
            edge.cost = scanner.nextInt();
            incedence.get(s).add(edge);
        }

        Tag[] costs = dijkstra(0, n, incedence);
        for (int i = 0; i < n; i++) {
            System.out.print(i + " = " + costs[i].cost + "; ");
            if (costs[i].cost < Integer.MAX_VALUE) {
                int j = costs[i].node;
                while (j >= 0) {
                    System.out.print(" <- " + j);
                    j = costs[j].node;
                }
            }
            System.out.println();
        }
    }
}
