public class GCD {

    // This method computes greatest common divisor by
    // Euclid's original subtraction method
    public static int withSubtractions(int a, int b) {

        while (a > 0 && b > 0) {
            if (a > b)
                a -=b;
            else
                b -= a;
        }
        return a + b;

    }

    // this method computes greatest common divisor by Recursive method
    public static int recursive(int a, int b) {

        if (b == 0)
            return a;
        else
            return recursive(b, a % b);
    }

    // this method computes greatest common divisor using Euclide's method
    public static int iterative(int a, int b) {

        while (b > 0) {
            int t = b;
            b = a % b;
            a = t;
        }
        return a + b;
    }


    public static void main(String[] args) {
        int a = 10;
        int b = 13;
        int d = withSubtractions(a, b);

        System.out.println("greatest commond divisor for " + a + " and " + b + " is " + d);
    }

}
