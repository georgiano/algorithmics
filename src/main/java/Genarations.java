public class Genarations {

    private static void printState(int[] state) {
        for (int i = 0; i < state.length; i++)
            System.out.print(state[i] + " ");
        System.out.println();
    }

    private static void permute(int[] state, int p, int N) {
        if (p == N) {
            printState(state);
            return;
        }

        for (int i = 1; i <= N; i++) {
            state[p] = i;
            permute(state, p + 1, N);
        }
    }


    public static void printRecursive(int N) {
        int[] state = new int[N];
        permute(state, 0, N);
    }


    public static void printNonRecursive(int N) {
        int state[] = new int[N];
        int p = 0;

        while (p >= 0) {
            if (p == N) {
                printState(state);
                p--;
                continue;
            }

            int c = state[p] + 1;
            if (c > N) {
                p--;
                continue;
            }
            state[p++] = c;
            if (p < N)
                state[p] = 0;
        }
    }


    public static void main(String[] args) {
        Genarations.printNonRecursive(3);
    }
}
