public class KMP {

    private static int[] calculateLPS(String s) {

        int[] lps = new int[s.length()];
        int i = 0;
        int l = 0;

        lps[i++] = 0;
        while (i < s.length())
            if (s.charAt(i) == s.charAt(l))
                lps[i++] = ++l;
            else {
                if (l > 0)
                    l = lps[l - 1];
                else
                    lps[i++] = 0;
            }

        return lps;
    }

    private static int _search(String s, String p, int[] lps) {
        int i = 0;
        int j = 0;

        while (i < s.length()) {
            if (s.charAt(i) == p.charAt(j)) {
                i++;
                j++;
                if (j == p.length())
                    return (i - j);
            } else {
                if (j == 0)
                    i++;
                else
                    j = lps[j - 1];
            }
        }
        return -1;
    }

    public static int search(String s, String p) {
        int[] lps = calculateLPS(p);
        return _search(s, p, lps);
    }



    private int[] lps;
    private String pattern;

    public KMP(String pattern) {
        this.pattern = pattern;
        lps = calculateLPS(pattern);
    }

    public int search(String s) {
        return _search(s, pattern, lps);
    }
}
