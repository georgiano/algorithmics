import java.util.ArrayList;
import java.util.List;

public class MinPriorityQueue implements PriorityQueue {
    private List<Integer> items;

    public MinPriorityQueue() {
        items = new ArrayList<Integer>();
    }


    public int size() {
        return items.size();
    }



    public void add(int v) {

        items.add(v);
        int i = items.size() - 1;
        int p = parent(i);

        while (p >= 0 && items.get(i) < items.get(p)) {
            swap(i, p);
            i = p;
            p = parent(p);
        }
    }


    public int extract() {

        int x = items.get(0);
        if (items.size() > 1) {
            items.set(0, items.remove(items.size() - 1));
            heapify(0);
        } else
            items.remove(0);
        return x;
    }


    private int parent(int i) {

        return (i + 1) / 2 - 1;
    }

    private void swap(int i, int j) {

        int t = items.get(i);
        items.set(i, items.get(j));
        items.set(j, t);
    }

    private void heapify(int i) {
        int n = items.size();

        int l = i * 2 + 1;
        int r = i * 2 + 2;

        int m = i;
        if (l < n && items.get(l) < items.get(m))
            m = l;
        if (r < n && items.get(r) < items.get(m))
            m = r;

        if (m != i) {
            swap(i, m);
            heapify(m);
        }
    }
}
