public interface PriorityQueue {
    public void add(int v);
    public int extract();
    public int size();
}
