package climbing_the_leaderboard;

import java.util.Scanner;

public class Solution {

    static int[] climbingLeaderboard(int[] scores, int[] alice) {

        int[] board = new int[scores.length];
        int[] res = new int[alice.length];
        int position = 1;
        int lastScore = scores[0];
        for (int i = 0; i < scores.length; i++) {
            board[i] = scores[i] < lastScore ? ++position : position;
            lastScore = scores[i];
        }

        position = board.length - 1;
        for (int i = 0; i < alice.length; i++) {

            while (position >=0 && scores[position] <= alice[i])
                position--;

            if (position < 0)
                res[i] = 1;
            else
                res[i] = board[position] + 1;
        }

        return res;
    }



    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] scores = new int[n];
        for(int scores_i = 0; scores_i < n; scores_i++){
            scores[scores_i] = in.nextInt();
        }
        int m = in.nextInt();
        int[] alice = new int[m];
        for(int alice_i = 0; alice_i < m; alice_i++){
            alice[alice_i] = in.nextInt();
        }
        int[] result = climbingLeaderboard(scores, alice);
        for (int i = 0; i < result.length; i++) {
            System.out.print(result[i] + (i != result.length - 1 ? "\n" : ""));
        }
        System.out.println("");


        in.close();
    }
}
