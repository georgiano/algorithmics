package codility.exam;

public class Problem1 {

    class Tree {
        public int x;
        public Tree l;
        public Tree r;
    }

    class Solution {
        private int[] counters;


        private int findDistincts(Tree T) {
            if (T == null)
                return 0;

            int c = 0;
            if (counters[T.x] == 0)
                c = 1;

            counters[T.x] ++;
            c += Math.max(findDistincts(T.l), findDistincts(T.r));
            counters[T.x] --;

            return c;
        }

        private int count(Tree T) {
            if (T == null)
                return 0;
            return 1 + count(T.l) + count(T.r);
        }

        public int solution(Tree T) {
            counters = new int[count(T) + 2];
            return findDistincts(T);
        }
    }



    private void test(Tree t) {
        Solution solution = new Solution();
        System.out.println(solution.solution(t));
    }


    public static void main(String[] args) {
        Problem1 task = new Problem1();
        task.test(null);
    }
}