package codility.exam;

public class Problem2 {


    class Solution {

        private int factorial(int n) {
            int f = 1;
            for (int i = 2; i <= n; i++)
                f = f * i;
            return f;
        }

        private int c(int n, int k) {
            if (n < k)
                return 1;
            return (factorial(n) / (factorial(k) * factorial (n - k)));
        }

        public int solution(int N) {
            if (N == 0)
                return 1;

            int[] count = new int[10];
            int digits = 0;
            while (N > 0) {
                int d = N % 10;
                N = N / 10;
                digits ++;

                count[d] ++;
            }

            int r = 1;
            int d = digits - count[0];
            for (int i = 1; i < 10; i++) {
                if (count[i] > 0) {
                    r = r * c(d, count[i]);
                    d -= count[i];
                }
            }


            if (count[0] > 0) {
                r = r * c(digits - 1 , count[0] );
            }

            return r;
        }
    }


    private void test(int n) {
        Solution solution = new Solution();
        System.out.println(n + "=" + solution.solution(n));
    }


    public static void main(String[] args) {
        Problem2 task = new Problem2();
        task.test(1213);
        task.test(1111);
        task.test(1113);
        task.test(100);
        task.test(110);
        task.test(1200);
        task.test(111100);
        task.test(0);
    }
}