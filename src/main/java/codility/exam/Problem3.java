package codility.exam;

public class Problem3 {


    class Solution {
        public int solution(String S) {
            return 0;
        }
    }


    private void test(String s) {
        Solution solution = new Solution();
        System.out.println(solution.solution(s));
    }


    public static void main(String[] args) {
        Problem3 task = new Problem3();
        task.test("abbabba");
        task.test("codility");
    }
}