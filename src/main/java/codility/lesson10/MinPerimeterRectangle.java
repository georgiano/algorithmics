/*
An integer N is given, representing the area of some rectangle.

The area of a rectangle whose sides are of length A and B is A * B, and the perimeter is 2 * (A + B).

The goal is to find the minimal perimeter of any rectangle whose area equals N.
The sides of this rectangle should be only integers.

For example, given integer N = 30, rectangles of area 30 are:

        (1, 30), with a perimeter of 62,
        (2, 15), with a perimeter of 34,
        (3, 10), with a perimeter of 26,
        (5, 6), with a perimeter of 22.

Write a function:

    class Solution { public int solution(int N); }

that, given an integer N, returns the minimal perimeter of any rectangle whose area is exactly equal to N.

For example, given an integer N = 30, the function should return 22, as explained above.

Assume that:

        N is an integer within the range [1..1,000,000,000].

Complexity:

        expected worst-case time complexity is O(sqrt(N));
        expected worst-case space complexity is O(1).
 */


package codility.lesson10;

public class MinPerimeterRectangle {


    class Solution {
        public int solution(int N) {

            int i = 1;
            int x = N;
            int min = (1 + N) * 2;
            do {
                x = N / i;
                if (i * x == N) {
                    if ((i + x) * 2 < min)
                        min = (i + x) * 2;

                }

                i++;
            } while (i <= x);
            return min;
        }
    }


    private void test(int N) {
        Solution solution = new Solution();
        System.out.println(solution.solution(N));
    }


    public static void main(String[] args) {
        MinPerimeterRectangle task = new MinPerimeterRectangle();
        task.test(30);
        task.test(1);
        task.test(982451653 );
    }
}