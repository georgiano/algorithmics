/*
wo positive integers N and M are given.
Integer N represents the number of chocolates arranged in a circle, numbered from 0 to N − 1.

You start to eat the chocolates. After eating a chocolate you leave only a wrapper.

You begin with eating chocolate number 0.
Then you omit the next M − 1 chocolates or wrappers on the circle, and eat the following one.

More precisely, if you ate chocolate number X, then you will next eat the chocolate
with number (X + M) modulo N (remainder of division).

You stop eating when you encounter an empty wrapper.

For example, given integers N = 10 and M = 4. You will eat the following chocolates: 0, 4, 8, 2, 6.

The goal is to count the number of chocolates that you will eat, following the above rules.
 */

package codility.lesson12;

public class ChocolatesByNumbers {

    class Solution {
        public int gcd(int N, int M) {
            while (N > 0 && M > 0) {
                if (N > M)
                    N = N % M;
                else
                    M = M % N;
            }

            return Math.max(N, M);
        }

        public int solution(int N, int M) {
            return N / gcd(N, M);
        }
    }



    private void test(int N, int M) {
        Solution solution = new Solution();

        int i = 0;
        int c = 0;
        do {
            i = (i + M) % N;
            c++;
        } while (i != 0);

        int sol = solution.solution(N, M);
        if (c != sol)
            System.out.print(N + ", " + M + " : " + c + "  ---> ");
    }


    public static void main(String[] args) {
        ChocolatesByNumbers task = new ChocolatesByNumbers();
        for (int n = 1; n < 1500; n++)
            for (int m = 1; m < 1500; m++)
                task.test(n, m);
    }
}