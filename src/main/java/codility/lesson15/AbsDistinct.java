/*
A non-empty zero-indexed array A consisting of N numbers is given.
The array is sorted in non-decreasing order.
The absolute distinct count of this array is the number of distinct absolute values among the elements of the array.

For example, consider array A such that:
  A[0] = -5
  A[1] = -3
  A[2] = -1
  A[3] =  0
  A[4] =  3
  A[5] =  6

The absolute distinct count of this array is 5, because there are 5 distinct absolute values
among the elements of this array, namely 0, 1, 3, 5 and 6.

Write a function:

    class Solution { public int solution(int[] A); }

that, given a non-empty zero-indexed array A consisting of N numbers, returns absolute distinct count of array A.

For example, given array A such that:
  A[0] = -5
  A[1] = -3
  A[2] = -1
  A[3] =  0
  A[4] =  3
  A[5] =  6

the function should return 5, as explained above.

Assume that:

        N is an integer within the range [1..100,000];
        each element of array A is an integer within the range [−2,147,483,648..2,147,483,647];
        array A is sorted in non-decreasing order.

Complexity:

        expected worst-case time complexity is O(N);
        expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).


 */
package codility.lesson15;

public class AbsDistinct {


    class Solution {
        private int shiftLeftPointer(int left, int[] A) {
            long value =  Math.abs((long)A[left]);
            while (left < A.length && Math.abs((long)A[left]) == value)
                left ++;
            return left;
        }

        private int shiftRightPointer(int right, int[] A) {
            long value =  Math.abs((long)A[right]);
            while (right >= 0 && Math.abs((long)A[right]) == value)
                right --;
            return right;
        }

        public int solution(int[] A) {
            int left = 0;
            int right = A.length - 1;

            int distincts = 0;
            while (left <= right && left < A.length && right >= 0) {
                long leftValue = Math.abs((long)A[left]);
                long rightValue = Math.abs((long)A[right]);
                if (leftValue > rightValue)
                    left = shiftLeftPointer(left, A);
                else
                if (leftValue < rightValue)
                    right = shiftRightPointer(right, A);
                else {
                    left = shiftLeftPointer(left, A);
                    right = shiftRightPointer(right, A);
                }
                distincts ++;
            }
            return distincts;
        }
    }


    private void test(int[] A) {
        Solution solution = new Solution();
        System.out.println(solution.solution(A));
    }


    public static void main(String[] args) {
        AbsDistinct task = new AbsDistinct();
        int[] A = new int[6];

        A[0] = -5;
        A[1] = -3;
        A[2] = -1;
        A[3] =  0;
        A[4] =  3;
        A[5] =  6;

        task.test(A);
        task.test(new int[]{1,2,3,4,5});
        task.test(new int[]{1});
        task.test(new int[]{-2147483648, -1, 0, 1});
    }
}