/*


This is a demo task.

Write a function:

    class Solution { public int solution(int[] A); }

that, given an array A of N integers, returns the smallest positive integer (greater than 0) that does not occur in A.

For example, given A = [1, 3, 6, 4, 1, 2], the function should return 5.

Given A = [1, 2, 3], the function should return 4.

Given A = [−1, −3], the function should return 1.

Assume that:

        N is an integer within the range [1..100,000];
        each element of array A is an integer within the range [−1,000,000..1,000,000].

Complexity:

        expected worst-case time complexity is O(N);
        expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).


 */

package codility.lesson4;

public class MissingInteger {


    class Solution {
        public int solution(int[] A) {

            int [] map = new int[A.length + 3];
            for (int i = 0; i < A.length; i++) {
                int v = A[i];
                if (v > 0 && v < map.length)
                    map[v] = 1;
            }

            for (int i = 1; i < map.length; i++)
                if (map[i] == 0)
                    return i;

            return 1;
        }
    }


    private void test(int[] A) {
        Solution solution = new Solution();
        System.out.println(solution.solution(A));
    }


    public static void main(String[] args) {
        MissingInteger task = new MissingInteger();
        int[] A = new int[]{1, 3, 6, 4, 1, 2};
        int[] B = new int[]{1, 2, 3};
        int[] C = new int[]{-1, -3};
        task.test(A);
        task.test(B);
        task.test(C);
    }
}