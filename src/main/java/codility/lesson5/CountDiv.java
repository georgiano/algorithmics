/*


Write a function:

    class Solution { public int solution(int A, int B, int K); }

that, given three integers A, B and K, returns the number of integers within the range [A..B] that are divisible by K, i.e.:

    { i : A ≤ i ≤ B, i mod K = 0 }

For example, for A = 6, B = 11 and K = 2, your function should return 3, because there are three numbers divisible by 2 within the range [6..11], namely 6, 8 and 10.

Assume that:

        A and B are integers within the range [0..2,000,000,000];
        K is an integer within the range [1..2,000,000,000];
        A ≤ B.

Complexity:

        expected worst-case time complexity is O(1);
        expected worst-case space complexity is O(1).


 */

package codility.lesson5;

public class CountDiv {


    class Solution {
        public int solution(int A, int B, int K) {
            int first =(int) ((((long) A) +  ((long) K) - 1) / K) ;
            int last = B / K;
            if (first > last)
                return 0;
            else
                return last - first + 1;

        }
    }


    private void test(int A, int B, int K) {
        Solution solution = new Solution();
        System.out.println(A + ".." + B + " | " + K + " :: " + solution.solution(A, B, K));
    }


    public static void main(String[] args) {
        CountDiv task = new CountDiv();
        task.test(6, 11, 2); // 6, 8, 10
        task.test(6, 11, 3); // 6, 9
        task.test(6, 12, 3); // 6, 9, 12
        task.test(7, 12, 3); // 9, 12
        task.test(7, 12, 13); // 0
        task.test(5, 11, 2); // 6, 8, 10
        task.test(0, 5, 2); // 6, 8, 10


        int []v = new int[]{1, Integer.MAX_VALUE};
        for (int a :v )
            for (int b:v)
                for (int k:v)
                    task.test(a, b, k);
    }
}