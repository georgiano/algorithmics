/*
A non-empty zero-indexed array A consisting of N integers is given.
A pair of integers (P, Q), such that 0 ≤ P < Q < N, is called a slice of array A (notice that the slice
contains at least two elements). The average of a slice (P, Q) is the sum of A[P] + A[P + 1] + ... + A[Q] divided
by the length of the slice. To be precise, the average equals (A[P] + A[P + 1] + ... + A[Q]) / (Q − P + 1).

For example, array A such that:
    A[0] = 4
    A[1] = 2
    A[2] = 2
    A[3] = 5
    A[4] = 1
    A[5] = 5
    A[6] = 8

contains the following example slices:

        slice (1, 2), whose average is (2 + 2) / 2 = 2;
        slice (3, 4), whose average is (5 + 1) / 2 = 3;
        slice (1, 4), whose average is (2 + 2 + 5 + 1) / 4 = 2.5.

The goal is to find the starting position of a slice whose average is minimal.

Write a function:

    class Solution { public int solution(int[] A); }

that, given a non-empty zero-indexed array A consisting of N integers, returns the starting position of the slice
with the minimal average. If there is more than one slice with a minimal average,
you should return the smallest starting position of such a slice.

For example, given array A such that:
    A[0] = 4
    A[1] = 2
    A[2] = 2
    A[3] = 5
    A[4] = 1
    A[5] = 5
    A[6] = 8

the function should return 1, as explained above.

Assume that:

        N is an integer within the range [2..100,000];
        each element of array A is an integer within the range [−10,000..10,000].

Complexity:

        expected worst-case time complexity is O(N);
        expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).

 */


package codility.lesson5;

public class MinAvgTwoSlice {


    class Solution {
        public int solution(int[] A) {

            long [] sums = new long[A.length + 1];
            int [] starts = new int[A.length + 1];

            sums[0] = A[0];
            sums[1] = A[0] + A[1];

            for (int i = 2; i < A.length; i++) {
                long x = A[i];

                long s1 = Long.valueOf(A[i - 1]) + x + sums[i - 2];
                long s2 = Long.valueOf(A[i - 1]) + x;

                if ((Double.valueOf(s2) / 2.0) < (Double.valueOf(s1) / (i - starts[i - 2] + 1))) {
                    sums[i] = s2;
                    starts[i] = i - 1;
                } else {
                    sums[i] = s1;
                    starts[i] = starts[i - 2];
                }
            }

            double min = Double.valueOf(sums[1]) / 2.0;
            int start = 0;

            for (int i = 2; i < A.length; i++) {
                double x = Double.valueOf(sums[i]) / (i - starts[i] + 1);

                if (x < min)  {
                    min = x;
                    start = starts[i];
                }
            }

            return start;
        }
    }


    private void test(int[] A) {
        Solution solution = new Solution();
        System.out.println(solution.solution(A));
    }


    public static void main(String[] args) {
        MinAvgTwoSlice task = new MinAvgTwoSlice();

        int[] A = new int[7];
        A[0] = 4;
        A[1] = 2;
        A[2] = 2;
        A[3] = 5;
        A[4] = 1;
        A[5] = 5;
        A[6] = 8;


        task.test(A);
    }
}