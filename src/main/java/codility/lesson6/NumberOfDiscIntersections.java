/*
The figure below shows discs drawn for N = 6 and A as follows:
  A[0] = 1
  A[1] = 5
  A[2] = 2
  A[3] = 1
  A[4] = 4
  A[5] = 0

There are eleven (unordered) pairs of discs that intersect, namely:

        discs 1 and 4 intersect, and both intersect with all the other discs;
        disc 2 also intersects with discs 0 and 3.

Write a function:

    class Solution { public int solution(int[] A); }

that, given an array A describing N discs as explained above, returns the number of (unordered) pairs of intersecting discs.
The function should return −1 if the number of intersecting pairs exceeds 10,000,000.

Given array A shown above, the function should return 11, as explained above.

Assume that:

        N is an integer within the range [0..100,000];
        each element of array A is an integer within the range [0..2,147,483,647].

Complexity:

        expected worst-case time complexity is O(N*log(N));
        expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
 */

package codility.lesson6;

import java.util.Arrays;
import java.util.Comparator;

public class NumberOfDiscIntersections {


    class Solution {
        private class Pixel {
            long x;
            char type;
            public Pixel (long x, char type) {
                this.x = x;
                this.type = type;
            }
        }

        public int solution(int[] A) {

            Pixel[] pixels = new Pixel[A.length * 2];
            for (int i = 0; i < A.length; i++) {
                pixels[i * 2] = new Pixel(Long.valueOf(i) - A[i], 'O');
                pixels[i * 2 + 1] = new Pixel(Long.valueOf(i) + A[i], 'C');
            }

            Arrays.sort(pixels, new Comparator<Pixel>() {
                public int compare(Pixel o1, Pixel o2) {
                    if (o1.x < o2.x)
                        return -1;
                    if (o1.x > o2.x)
                        return 1;

                    if (o1.type == 'O')
                        return -1;

                    if (o1.type == 'C')
                        return 1;

                    return 0;
                }
            });

            long openCircles = 0;
            long intersections = 0;

            for (Pixel p : pixels) {
                if (p.type == 'O') {
                    intersections += openCircles;
                    openCircles ++;
                } else
                    openCircles --;

                if (intersections > 10000000)
                    return -1;
            }
            return (int)intersections;
        }
    }


    private void test(int[] A) {
        Solution solution = new Solution();
        System.out.println(solution.solution(A));
    }


    public static void main(String[] args) {
        NumberOfDiscIntersections task = new NumberOfDiscIntersections();

        int[] A = new int[6];
        A[0] = 1;
        A[1] = 5;
        A[2] = 2;
        A[3] = 1;
        A[4] = 4;
        A[5] = 0;
        task.test(A);

        int[] B = new int[]{1, 2147483647, 0};
        task.test(B);
    }
}