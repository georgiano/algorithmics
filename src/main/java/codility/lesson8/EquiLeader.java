/*
A non-empty zero-indexed array A consisting of N integers is given.

The leader of this array is the value that occurs in more than half of the elements of A.

An equi leader is an index S such that 0 ≤ S < N − 1 and two sequences
A[0], A[1], ..., A[S] and A[S + 1], A[S + 2], ..., A[N − 1] have leaders of the same value.

For example, given array A such that:
    A[0] = 4
    A[1] = 3
    A[2] = 4
    A[3] = 4
    A[4] = 4
    A[5] = 2

we can find two equi leaders:

        0, because sequences: (4) and (3, 4, 4, 4, 2) have the same leader, whose value is 4.
        2, because sequences: (4, 3, 4) and (4, 4, 2) have the same leader, whose value is 4.

The goal is to count the number of equi leaders.

Write a function:

    class Solution { public int solution(int[] A); }

that, given a non-empty zero-indexed array A consisting of N integers, returns the number of equi leaders.

For example, given:
    A[0] = 4
    A[1] = 3
    A[2] = 4
    A[3] = 4
    A[4] = 4
    A[5] = 2

the function should return 2, as explained above.

Assume that:

        N is an integer within the range [1..100,000];
        each element of array A is an integer within the range [−1,000,000,000..1,000,000,000].

Complexity:

        expected worst-case time complexity is O(N);
        expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).


 */
package codility.lesson8;

public class EquiLeader {

    class Solution {
        public int solution(int[] A) {

            int n = A.length;
            if (n < 2)
                return 0;

            int sp = 0;
            int candidate = 0;
            for (int i = 0; i < n; i++) {
                int value = A[i];

                if (sp == 0) {
                    candidate = A[i];
                    sp++;
                    continue;
                }

                if (candidate != value) {
                    sp--;
                    continue;
                } else
                    sp++;
            }

            int occurances[] = new int[n];
            occurances[0] = A[0] == candidate ? 1 : 0;
            for (int i = 1; i < n; i++)
                occurances[i] = occurances[i - 1] + (A[i] == candidate ? 1 : 0);

            int count = 0;
            for (int i = 0; i < n - 1; i++) {
                int leftSize = i + 1;
                int rightSize = n - leftSize;

                int leftOccurances = occurances[i];
                int rightOccurances = occurances[n - 1] - leftOccurances;

                if (leftOccurances * 2 > leftSize && rightOccurances * 2 > rightSize)
                    count++;
            }

            return count;
        }
    }


    private void test(int[] A) {
        Solution solution = new Solution();
        System.out.println(solution.solution(A));
    }


    public static void main(String[] args) {
        EquiLeader task = new EquiLeader();

        task.test(new int[]{4, 3, 4, 4, 4, 2});
        task.test(new int[]{3, 4, 5});
        task.test(new int[]{3, 4, 5, 5});
        task.test(new int[]{2, 1, 1, 1, 3});

    }}