/*
A non-empty zero-indexed array A consisting of N integers is given. A pair of integers (P, Q),
such that 0 ≤ P ≤ Q < N, is called a slice of array A.
The sum of a slice (P, Q) is the total of A[P] + A[P+1] + ... + A[Q].

Write a function:

    class Solution { public int solution(int[] A); }

that, given an array A consisting of N integers, returns the maximum sum of any slice of A.

For example, given array A such that:
A[0] = 3  A[1] = 2  A[2] = -6
A[3] = 4  A[4] = 0

the function should return 5 because:

        (3, 4) is a slice of A that has sum 4,
        (2, 2) is a slice of A that has sum −6,
        (0, 1) is a slice of A that has sum 5,
        no other slice of A has sum greater than (0, 1).

Assume that:

        N is an integer within the range [1..1,000,000];
        each element of array A is an integer within the range [−1,000,000..1,000,000];
        the result will be an integer within the range [−2,147,483,648..2,147,483,647].

Complexity:

        expected worst-case time complexity is O(N);
        expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).

 */
package codility.lesson9;

public class MaxSliceSum {


    class Solution {
        public int solution(int[] A) {

            int n = A.length;
            long maxSum = Integer.MIN_VALUE;
            long sum = Integer.MIN_VALUE;
            for (int i = 0; i < n; i++) {
                sum = Math.max(0L, sum) + A[i];
                maxSum = Math.max(sum, maxSum);
            }
            return (int) maxSum;
        }
    }



    private void test(int[] A) {
        Solution solution = new Solution();
        System.out.println(solution.solution(A));
    }


    public static void main(String[] args) {
        MaxSliceSum task = new MaxSliceSum();
        int[] A = new int[5];
        A[0] = 3;  A[1] = 2;  A[2] = -6;
        A[3] = 4;  A[4] = 0;

        task.test(A);
        task.test(new int[]{-500});
    }
}