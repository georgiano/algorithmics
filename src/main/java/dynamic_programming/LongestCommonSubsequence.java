package dynamic_programming;

public class LongestCommonSubsequence {

    public static int lcs(String s1, String s2) {

        int n = s1.length();
        int m = s2.length();

        int lcs[][] = new int[n + 1][m + 1];

        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= m; j++) {
                if (s1.charAt(i - 1) == s2.charAt(j - 1))
                    lcs[i][j] = lcs[i - 1][ j - 1] + 1;
                else
                    lcs[i][j] = Math.max(lcs[i - 1][j], lcs[i][j - 1]);
            }
        }

        return lcs[n][m];
    }


    public static void main(String args[]) {
        System.out.println(lcs("AGGTAB", "GXTXAYB"));   // 4: GTAB
        System.out.println(lcs("ABCDGH", "AEDFHR"));    // 3: ADH
        System.out.println(lcs("AGGTAB", "GXTXAYB"));   // 4: GTAB
    }
}
