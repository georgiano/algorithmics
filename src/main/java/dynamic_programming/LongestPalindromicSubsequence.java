package dynamic_programming;

public class LongestPalindromicSubsequence {

    public static int lps(String str) {

        int n = str.length();

        // Create a table to store intermediate calculations
        int lps[][] = new int[n][n];
        for (int i = 0; i < n; i++)
            lps[i][i] = 1;

        for (int len = 2; len <= n; len++) {
            for (int start = 0; start < n - len + 1; start++) {
                int end = start + len - 1;
                if (str.charAt(start) == str.charAt(end)) {
                    if (len == 2)
                        lps[start][end] = 2;
                    else
                        lps[start][end] = lps[start + 1][end - 1] + 2;
                } else
                    lps[start][end] = Math.max(lps[start][end - 1], lps[start + 1][end]);
            }
        }

        return lps[0][n - 1];
    }

    public static void main(String args[]) {
        String seq = "GEEKSFORGEEKS";
        System.out.println("The length of the longest palindromic subsequence is " + lps(seq));
    }
}
