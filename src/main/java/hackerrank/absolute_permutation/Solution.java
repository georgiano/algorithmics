package hackerrank.absolute_permutation;

import java.util.Scanner;

public class Solution {

    private static void printPermutation(int n, int k) {
        int[] map = new int[n + 1];
        String space = "";
        StringBuilder res = new StringBuilder();
        for (int i = 1; i <= n; i++) {
            int x1 = i + k;
            int x2 = i - k;

            if (x2 > 0 && map[x2] == 0) {
                map[x2] = 1;
                res.append(space + x2);
                space = " ";
            } else if (x1 <= n  && map[x1] == 0) {
                map[x1] = 1;
                res.append(space + x1);
                space = " ";
            } else {
                res = new StringBuilder("-1");
                break;
            }

        }

        System.out.println(res.toString());
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        for (int a0 = 0; a0 < t; a0++) {
            int n = in.nextInt();
            int k = in.nextInt();
            printPermutation(n, k);
        }
    }
}
