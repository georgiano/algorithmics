package hackerrank.bigger_is_greater;

import java.util.Scanner;

public class Solution {

    public static final String NO_ANSWER = "no answer";

    static String biggerIsGreater(String w) {

        int[] counters = new int['z' - 'a' + 1];

        if (w == null || w.length() == 0)
            return NO_ANSWER;

        counters[index(w.charAt(w.length() - 1))] = 1;

        for (int i = w.length() - 2; i >= 0; i--) {
            if (w.charAt(i) >= w.charAt(i + 1)) {
                counters[index(w.charAt(i))]++;
                continue;
            }

            int x = index(w.charAt(i));
            int n = x + 1;
            while (n < counters.length && counters[n] == 0)
                n++;
            if (n > counters.length)
                return NO_ANSWER;

            String s = (i > 0 ? w.substring(0, i) : "") + String.valueOf((char)('a' + n));

            counters[n] --;
            counters[x] ++;


            for (int c = 0; c < counters.length; c++)
                for (int j = 0; j < counters[c]; j++)
                    s+= String.valueOf((char)('a' + c));

            return s;

        }

        return NO_ANSWER;
    }


    private static int index(char c) {
        return c - 'a';
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int T = in.nextInt();
        for(int a0 = 0; a0 < T; a0++){
            String w = in.next();
            String result = biggerIsGreater(w);
            System.out.println(result);
        }
        in.close();
    }
}
