package hackerrank.coin_change;

import java.util.Scanner;

public class Solution {

    public static long exchanges(long[][] mem, long amount, long[] coins, int index) {

        if (amount == 0)
            return 1;

        if (index == coins.length)
            return 0;

        long count = 0;
        for (long i = 0; i * coins[index] <= amount; i++) {

            long leftAmount = amount - i * coins[index];
            if (mem[(int) leftAmount][index + 1] >= 0)
                count += mem[(int) leftAmount][index + 1];
            else
                count += exchanges(mem,leftAmount,  coins, index + 1);
        }
        mem[(int) amount][index] = count;
        return count;

    }

    private static long solveRecursively(long n, long[] c) {

        long[][] mem = new long[(int) (n + 1)][c.length + 1];
        for (int a = 0; a <= n; a++)
            for (int m = 0; m <= c.length; m++)
                mem[a][m] = -1;

        return exchanges(mem, n, c, 0);
    }


    private static long solveIteratively(long n, long[] c) {

        long[] mem = new long[(int) (n + 1)];
        mem[0] = 1;

        for (int coin = 0; coin < c.length; coin++) {
            for (int a = 0; a + c[coin] <= n; a++)
                if (mem[a] > 0)
                    mem[(int) (a + c[coin])] += mem[a];
        }

        return mem[(int) n];
    }

    static long getWays(long n, long[] c){
        return solveIteratively(n, c);
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int m = in.nextInt();
        long[] c = new long[m];
        for(int c_i=0; c_i < m; c_i++){
            c[c_i] = in.nextLong();
        }
        // Print the number of ways of making change for 'n' units using coins having the values given by 'c'
        long ways = getWays(n, c);
        System.out.println(ways);
    }
}
