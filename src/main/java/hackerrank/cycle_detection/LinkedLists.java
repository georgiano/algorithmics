package hackerrank.cycle_detection;

import com.sun.org.apache.xpath.internal.axes.ReverseAxesWalker;
import com.sun.scenario.effect.Merge;

public class LinkedLists {

    static class Node {
        int data;
        Node next;
    }

    boolean hasCycle(Node head) {
        Node slow = head;
        Node fast = head;

        while (fast != null && fast.next != null) {
            fast = fast.next.next;
            slow = slow.next;

            if (fast == slow)
                return true;
        }

        return false;

    }


    public int countItems(Node head) {
        int res = 0;
        while (head != null) {
            res++;
            head = head.next;
        }
        return res;
    }


    private int mergeNode(int diff, Node nodeA, Node nodeB) {
        while (diff > 0) {
            nodeA = nodeA.next;
            diff--;
        }

        while (nodeA != nodeB) {
            nodeA = nodeA.next;
            nodeB = nodeB.next;
        }

        return nodeA.data;
    }


    int FindMergeNode(Node headA, Node headB) {
        int sizeA = countItems(headA);
        int sizeB = countItems(headB);

        int diff = Math.abs(sizeA - sizeB);
        if (sizeA > sizeB)
            return mergeNode(diff, headA, headB);
        else
            return mergeNode(diff, headB, headA);

    }

    private static void printList(Node head) {
        while (head != null) {
            System.out.print(head.data + " -> ");
            head = head.next;
        }
        System.out.println("NULL");
    }

    static Node Reverse(Node head) {

        Node prev = null;
        while (head != null) {
            Node tmp = head.next;
            head.next = prev;
            prev = head;
            head = tmp;
        }

        return prev;
    }


    public static Node mergeLists(Node headA, Node headB) {


        if (headA == null)
            return headB;
        if (headB == null)
            return headA;

        Node node;

        if (headA.data < headB.data) {
            node = headA;
            headA = headA.next;
        } else {
            node = headB;
            headB = headB.next;
        }
        Node head = node;

        while (true) {
            if (headA == null) {
                node.next = headB;
                break;
            }
            if (headB == null) {
                node.next = headA;
                break;
            }

            if (headA.data < headB.data) {
                node.next = headA;
                headA = headA.next;
            } else {
                node.next = headB;
                headB = headB.next;
            }
            node = node.next;
        }

        return head;
    }

    int CompareLists(Node headA, Node headB) {
        while (headA != null && headB != null) {
            if (headA.data != headB.data)
                return 0;
            headA = headA.next;
            headB = headB.next;

        }

        if (headA != null || headB != null)
            return 0;

        return 1;
    }

    public static int GetNode(Node head, int n) {
        Node node = head;
        for (int i = 0; i < n ; i++)
            head = head.next;

        while (head.next != null) {
            node = node.next;
            head = head.next;
        }

        return node.data;
    }


    public static Node RemoveDuplicates(Node head) {


        Node node = head;

        while (node != null && node.next != null) {
            if (node.data == node.next.data) {
                // cut out node.next
                node.next = node.next.next;
            } else
                node = node.next;

        }

        return head;


    }

    public static void main(String[] args) {
        Node a = new Node();
        Node b = new Node();
        Node c = new Node();

        a.data = 1; a.next = b;
        b.data = 2; b.next = c;
        c.data = 4;

        Node d = new Node();
        Node e = new Node();
        d.data = 2; d.next = e;
        e.data = 6;

        printList(a);
        printList(d);
        Node merged = mergeLists(a, d);
        printList(merged);
        System.out.println();

        System.out.println(0 + " -> " + GetNode(merged, 0));
        System.out.println(1 + " -> " + GetNode(merged, 1));
        System.out.println(4 + " -> " + GetNode(merged, 4));

        printList(RemoveDuplicates(merged));

    }




}