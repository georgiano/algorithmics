package hackerrank.encryption;

import java.util.Scanner;

public class Solution {

    static String encryption(String s) {
        int n = s.length();
        int sqr = (int) Math.sqrt(n);
        int rows = sqr;
        int cols = sqr;

        if (rows * cols < n)
            cols++;
        if (rows * cols < n)
            rows++;

        StringBuffer buf = new StringBuffer();

        String space = "";
        for (int c = 0; c < cols; c++) {
            buf.append(space);
            for (int r = 0; r < rows; r++) {
                int i = r * cols + c;
                if (i < n)
                    buf.append(s.charAt(i));
            }
            space = " ";
        }
        return buf.toString();
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.next();
        String result = encryption(s);
        System.out.println(result);
        in.close();
    }
}
