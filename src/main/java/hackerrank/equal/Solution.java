package hackerrank.equal;

import java.util.Arrays;
import java.util.Scanner;

public class Solution {

    private static int ops(int[]arr, int base) {
        int cnt = 0;
        for (int i = 0; i < arr.length; i++) {
            int diff = arr[i] - base;
            if (diff > 0) {

                cnt += diff / 5;
                diff = diff % 5;

                cnt += diff / 2;
                diff = diff % 2;

                cnt += diff;
            }
        }
        return cnt;
    }


    static int equal(int[] arr) {
        Arrays.sort(arr);

        int min = arr[0];
        for (int i = 1; i < arr.length; i++)
            if (arr[i] < min)
                min = arr[i];

        int ops = ops(arr, min);
        for (int i = 1; i < 5; i++) {
            int d = ops(arr, min - i);
            if (d < ops)
                ops = d;
        }

        return ops;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        for(int a0 = 0; a0 < t; a0++){
            int n = in.nextInt();
            int[] arr = new int[n];
            for(int arr_i = 0; arr_i < n; arr_i++){
                arr[arr_i] = in.nextInt();
            }
            int result = equal(arr);
            System.out.println(result);
        }
        in.close();
    }
}
