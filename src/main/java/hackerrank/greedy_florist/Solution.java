package hackerrank.greedy_florist;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    static int getMinimumCost(int n, int k, int[] c) {
        Arrays.sort(c);
        for (int i = 0; i < n / 2; i++) {
            int t = c[i];
            c[i] = c[n - i - 1];
            c[n - i - 1] = t;
        }
        int price = 0;
        for (int i = 0; i < n; i ++)
            price += ((i / k) + 1) * c[i];

        return price;
    }

    public static void main(String[] args) {

        for (int i = 0; i < 100; i++)
            if ((i + 7 - 1) / 7 != i / 7)
                System.out.println(i);
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
        int[] c = new int[n];
        for(int c_i=0; c_i < n; c_i++){
            c[c_i] = in.nextInt();
        }
        int minimumCost = getMinimumCost(n, k, c);
        System.out.println(minimumCost);
    }
}
