package hackerrank.journey_to_the_moon;

import java.util.*;

public class Solution {

    static long countConationals(TreeSet<Integer> astronauts, List<List<Integer>> incidence, int a) {
        Queue<Integer> queue = new LinkedList<Integer>();

        long cnt = 1;
        queue.add(a);
        astronauts.remove(a);

        while (queue.size() > 0) {
            int a1 = queue.poll();
            List<Integer> lst = incidence.get(a1);
            for (int i = 0; i < lst.size(); i++) {
                int a2 = lst.get(i);
                if  (astronauts.contains(a2)) {
                    astronauts.remove(a2);
                    queue.offer(a2);
                    cnt++;
                }
            }
        }

        return cnt;
    }


    static long journeyToMoon(int n, int[][] astronaut) {

        // build incidence matrix
        TreeSet<Integer> astronauts = new TreeSet<Integer>();
        List<List<Integer>> incidence = new ArrayList<List<Integer>>();
        for (int i = 0; i < n; i++) {
            incidence.add(new ArrayList<Integer>());
            astronauts.add(i);
        }

        for (int i = 0; i < astronaut.length; i++) {
            int a1 = astronaut[i][0];
            int a2 = astronaut[i][1];

            incidence.get(a1).add(a2);
            incidence.get(a2).add(a1);
        }

         /*
         0
         a * b
         a * b + a * c + b * c = a * b + (a + b) * c
         a * b + a * c + b * c + a * d + b * d + c * d = a * b + (a + b) * c + (a + b + c) * d
         */

        long currResult = 0;
        long prevSum = 0;

        while (!astronauts.isEmpty()) {
            int a = astronauts.first();
            long cnt = countConationals(astronauts, incidence, a);

            currResult += prevSum * cnt;
            prevSum += cnt;

        }

        return currResult;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int p = in.nextInt();
        int[][] astronaut = new int[p][2];
        for(int astronaut_i = 0; astronaut_i < p; astronaut_i++){
            for(int astronaut_j = 0; astronaut_j < 2; astronaut_j++){
                astronaut[astronaut_i][astronaut_j] = in.nextInt();
            }
        }
        long result = journeyToMoon(n, astronaut);
        System.out.println(result);
        in.close();
    }
}
