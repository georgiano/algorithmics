package hackerrank.kaprekar_numbers;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Solution {

    private static int digits(long x) {
        int modulus = 1;
        if (x < 10)
            modulus = 10;
        else if (x < 100)
            modulus = 100;
        else if (x < 1000)
            modulus = 1000;
        else if (x < 10000)
            modulus = 10000;
        else if (x < 100000)
            modulus = 100000;
        else
            modulus = 10000000;

        return modulus;
    }

    private static boolean kaprekar(long x) {
        long modulus = digits(x);
        long n = x * x;
        long a = n / modulus;
        long b = n % modulus;

        if (b > 0 && a + b == x)
            return true;
        else
            return false;

    }

    static List<Integer> kaprekarNumbers(int p, int q) {
        List<Integer> list = new ArrayList<Integer>();
        for (int i = p; i <= q; i++)
            if (kaprekar(i)) {
                list.add(i);
            }
        return list;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int p = in.nextInt();
        int q = in.nextInt();
        List<Integer> result = kaprekarNumbers(p, q);
        if (result.size() == 0)
            System.out.println("INVALID RANGE");
        else {
            String space = "";
            for (Integer k : result) {
                System.out.print(space + k);
                space = " ";
            }
            System.out.println("");
        }

        in.close();
    }
}
