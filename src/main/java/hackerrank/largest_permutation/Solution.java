package hackerrank.largest_permutation;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    static int[] largestPermutation(int k, int[] arr) {

        int pos = 0;
        int swap = 0;
        int n = arr.length;
        while (pos < n && swap < k) {
            int max = n - pos;
            if (arr[pos] >= max) {
                pos ++;
                continue;
            }

            for (int i = pos + 1; i < n; i++)
                if (arr[i] == max) {
                    int tmp = arr[i];
                    arr[i] = arr[pos];
                    arr[pos] = tmp;

                    pos++;
                    swap++;
                    break;
                }
        }

        return arr;
    }



    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
        int[] arr = new int[n];
        for(int arr_i = 0; arr_i < n; arr_i++){
            arr[arr_i] = in.nextInt();
        }


//        int k =  1;
//        int[] arr = new int[]{4, 2, 3, 5, 1};

        int[] result = largestPermutation(k, arr);
        for (int i = 0; i < result.length; i++) {
            System.out.print(result[i] + (i != result.length - 1 ? " " : ""));
        }
        System.out.println("");


        in.close();
    }
}
