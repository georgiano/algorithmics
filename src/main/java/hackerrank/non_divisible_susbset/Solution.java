package hackerrank.non_divisible_susbset;

import java.util.Scanner;

public class Solution {

    static int nonDivisibleSubset(int k, int[] arr) {

        int[] mods = new int[k + 1];
        for (int i = 0; i < arr.length; i++)
            mods[arr[i] % k] ++;
        mods[0] = Math.min(1, mods[0]);
        if (k % 2 == 0)
            mods[k / 2] = Math.min(1, mods[k / 2]);

        int sum = 0;
        for (int i = 0; i <= k / 2; i++) {
            int c = k - i;
            sum += Math.max(mods[i], mods[c]);
        }
        return sum;
    }


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
        int[] arr = new int[n];
        for(int arr_i = 0; arr_i < n; arr_i++){
            arr[arr_i] = in.nextInt();
        }
        int result = nonDivisibleSubset(k, arr);
        System.out.println(result);
        in.close();
    }
}
