package hackerrank.organizing_containers_of_balls;

import java.util.Arrays;
import java.util.Scanner;

public class Solution {

    static String organizingContainers(int[][] container) {
        int n = container[0].length;
        long[] balls = new long[n];
        long[] counters = new long[n];

        for (int c = 0; c < n; c++) {
            for (int b = 0; b < n; b++) {
                balls[c] += container[c][b];
                counters[b] += container[c][b];
            }
        }
        Arrays.sort(balls);
        Arrays.sort(counters);
        for (int i = 0; i < n; i++)
            if (balls[i] != counters[i])
                return "Impossible";

        return "Possible";
    }


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int q = in.nextInt();
        for(int a0 = 0; a0 < q; a0++){
            int n = in.nextInt();
            int[][] container = new int[n][n];
            for(int container_i = 0; container_i < n; container_i++){
                for(int container_j = 0; container_j < n; container_j++){
                    container[container_i][container_j] = in.nextInt();
                }
            }
            String result = organizingContainers(container);
            System.out.println(result);
        }
        in.close();
    }
}
