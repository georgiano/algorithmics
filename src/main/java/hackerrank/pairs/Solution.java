package hackerrank.pairs;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    static int pairs(int k, int[] arr) {
        Arrays.sort(arr);

        int s = 0;
        int e = 0;
        int count = 0;
        while (s < arr.length && e <= s) {
            long diff = Math.abs(((long) arr[s]) - ((long) arr[e]));
            if (diff == k) {
                count ++;
                e ++;
                s ++;
            } else
                if (diff < k)
                    s ++;
            else
                e ++;
        }

        return count;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
        int[] arr = new int[n];
        for(int arr_i = 0; arr_i < n; arr_i++){
            arr[arr_i] = in.nextInt();
        }
        int result = pairs(k, arr);
        System.out.println(result);
        in.close();
    }
}
