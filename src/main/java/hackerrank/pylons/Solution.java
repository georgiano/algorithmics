package hackerrank.pylons;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    static int pylons(int k, int[] arr) {
        int res = 0;

        int i = 0;
        int n = arr.length;
        int lastTower = -1000;
        while (i < n) {

            for (int j = i; j < Math.min(n, i + k); j ++)
                if (arr[j] > 0)
                    lastTower = j;

                if (Math.abs(i - lastTower) >= k)
                    return -1;

                res ++;
                i = lastTower + k;

                if (i < n)
                    for (int j = i; j > lastTower; j --)
                        if (arr[j] > 0) {
                            lastTower = j;
                            break;
                        }
        }

        return res;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
        int[] arr = new int[n];
        for(int arr_i = 0; arr_i < n; arr_i++){
            arr[arr_i] = in.nextInt();
        }
        int result = pylons(k, arr);
        System.out.println(result);
        in.close();
    }
}
