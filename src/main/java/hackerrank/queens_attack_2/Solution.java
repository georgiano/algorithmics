package hackerrank.queens_attack_2;

import java.util.Scanner;

public class Solution {

    static int queensAttack(int n, int k, int qy, int qx, int[][] obstacles) {

        int v1 = n - qy;
        int v2 = qy - 1;
        int h1 = n - qx;
        int h2 = qx - 1;

        int d1 = Math.min(h1, v1);
        int d2 = Math.min(h1, v2);
        int d3 = Math.min(h2, v2);
        int d4 = Math.min(h2, v1);

        for (int i = 0; i < k; i++) {
            int x = obstacles[i][1];
            int y = obstacles[i][0];

            if (x == qx && y > qy)
                v1 = Math.min(v1, y - qy - 1);

            if (y == qy && x > qx)
                h1 = Math.min(h1, x - qx - 1);

            if (x == qx && y < qy)
                v2 = Math.min(v2, qy - y - 1);

            if (y == qy && x < qx)
                h2 = Math.min(h2, qx - x - 1);

            if (Math.abs(qx - x) == Math.abs(qy - y)) {

                if (x > qx && y > qy)
                    d1 = Math.min(d1, x - qx - 1);

                if (x > qx && y < qy)
                    d2 = Math.min(d2, x - qx - 1);

                if (x < qx && y < qy)
                    d3 = Math.min(d3, qx - x - 1);

                if (x < qx && y > qy)
                    d4 = Math.min(d4, qx - x - 1);
            }
        }

        return v1 + v2 + h1 + h2 + d1 + d2 + d3 + d4;
    }


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
        int x_q = in.nextInt();
        int y_q = in.nextInt();
        int[][] obstacles = new int[k][2];
        for(int obstacles_i = 0; obstacles_i < k; obstacles_i++){
            for(int obstacles_j = 0; obstacles_j < 2; obstacles_j++){
                obstacles[obstacles_i][obstacles_j] = in.nextInt();
            }
        }
        int result = queensAttack(n, k, x_q, y_q, obstacles);
        System.out.println(result);
        in.close();
    }
}
