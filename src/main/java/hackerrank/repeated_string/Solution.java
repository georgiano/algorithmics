package hackerrank.repeated_string;

import java.util.Scanner;

public class Solution {

    static long repeatedString(String s, long n) {
        if (n == 0 || s.length() == 0)
            return 0;

        long sum = 0;
        long reps = n / s.length();
        long mod = n % s.length();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == 'a') {
                sum += reps;
                if (i < mod)
                    sum ++;
            }
        }
        return sum;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.next();
        long n = in.nextLong();
        long result = repeatedString(s, n);
        System.out.println(result);
        in.close();
    }
}
