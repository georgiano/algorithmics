package hackerrank.sherlock_and_cost;

import java.util.Scanner;

public class Solution {

    static int cost(int[] arr) {
        int[] lo = new int[arr.length + 1];
        int[] hi = new int[arr.length + 1];

        for (int i = 1; i < arr.length; i++) {
            int ll = 0;
            int lh = arr[i] - 1;
            int hl = Math.abs(arr[i - 1] - 1);
            int hh = Math.abs(arr[i - 1] - arr[i]);

            lo[i] = Math.max(lo[i - 1] + ll, hi[i - 1] + hl);
            hi[i] = Math.max(lo[i - 1] + lh, hi[i - 1] + hh);
        }

        return Math.max(lo[arr.length - 1], hi[arr.length - 1]);
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        for(int a0 = 0; a0 < t; a0++){
            int n = in.nextInt();
            int[] arr = new int[n];
            for(int arr_i = 0; arr_i < n; arr_i++){
                arr[arr_i] = in.nextInt();
            }
            int result = cost(arr);
            System.out.println(result);
        }
        in.close();
    }
}
