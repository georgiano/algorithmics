package hackerrank.synchronous_shopping;

import java.util.*;

public class Solution {

    static class Trip {
        int dst;
        long time;
    }

    static class Task {
        int src;
        int fishes;
    }

    private static long solution(int n, int m, int k, int[] fishmonger, List<List<Trip>> incidence) {

        int fullFish = (1 << k) - 1;

        long[][] matrix = new long[n + 1][fullFish + 1];
        for (int i = 0; i <= n; i++)
            for (int j = 0; j < matrix[i].length; j++)
                matrix[i][j] = Long.MAX_VALUE;

        Queue<Task> queue = new LinkedList<Task>();

        Task task = new Task();
        task.src = 1;
        task.fishes = fishmonger[1];
        matrix[1][fishmonger[1]] = 0;
        queue.offer(task);

        while (!queue.isEmpty()) {
            task = queue.poll();
            List<Trip> edgeList = incidence.get(task.src);
            for (int i = 0; i < edgeList.size(); i++) {
                Trip trip = edgeList.get(i);

                long time = matrix[task.src][task.fishes] + trip.time;
                int fishes = task.fishes | fishmonger[trip.dst];

                if (matrix[trip.dst][fishes] > time) {
                    matrix[trip.dst][fishes] = time;
                    Task newTask = new Task();
                    newTask.src = trip.dst;
                    newTask.fishes = fishes;
                    queue.offer(newTask);
                }
            }
        }

        long min = Long.MAX_VALUE;
        for (int i = 1; i <= fullFish; i++)
            for (int j = i; j <= fullFish; j++)
                if ((i | j) == fullFish && (Math.max(matrix[n][i],matrix[n][j]) < min))
                    min = Math.max(matrix[n][i], matrix[n][j]);
        return min;
    }

    public static void main(String[] args) {

//        Scanner scanner = new Scanner("5 5 5\n" +
//                "1 1\n" +
//                "1 2\n" +
//                "1 3\n" +
//                "1 4\n" +
//                "1 5\n" +
//                "1 2 10\n" +
//                "1 3 10\n" +
//                "2 4 10\n" +
//                "3 5 10\n" +
//                "4 5 10\n");


        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int m = scanner.nextInt();
        int k = scanner.nextInt();

        // read fishmongers
        int[] fishmonger = new int[n + 1];
        for (int i = 1; i <= n; i++) {
            int f = scanner.nextInt();
            for (int j = 0; j < f; j++) {
                int t = scanner.nextInt();
                if (t != 0)
                    fishmonger[i] = fishmonger[i] | (1 << (t - 1));
            }
        }

        // read roads
        List<List<Trip>> incidence = new ArrayList<List<Trip>>();
        for (int i = 0; i <= n; i++) {
            incidence.add(new ArrayList<Trip>());
        }

        for (int i = 0; i < m; i++) {
            int s = scanner.nextInt();
            int d = scanner.nextInt();
            int t = scanner.nextInt();

            Trip ts= new Trip();
            ts.dst = d;
            ts.time = t;
            incidence.get(s).add(ts);

            Trip td= new Trip();
            td.dst = s;
            td.time = t;
            incidence.get(d).add(td);
        }

        System.out.println(solution(n, m, k, fishmonger, incidence));

    }
}
