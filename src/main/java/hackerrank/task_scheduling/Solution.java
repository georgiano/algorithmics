package hackerrank.task_scheduling;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    static class Task {
        int time;
        int deadline;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int tasks = scanner.nextInt();
        Task[] task = new Task[tasks];

        for (int i = 0; i < tasks; i++) {
            task[i] = new Task();
            task[i].deadline = scanner.nextInt();
            task[i].time = scanner.nextInt();
        }
        scanner.close();

        Arrays.sort(task, new Comparator<Task>() {
            public int compare(Task o1, Task o2) {
                return Integer.compare(o1.deadline, o2.deadline);
            }
        });


        int cnt = 0;
        int t = 0;
        for (int i = 0; i < tasks; i++) {
            if (t + task[i].time <= task[i].deadline) {
                cnt ++;
                t = task[i].deadline - (t + task[i].time);
            }
        }
        System.out.println(cnt);

    }
}