package sorting;

public class HeapSort implements Sorter {

    public int[] sort(int[] a) {

        // build heap
        buildHeap(a, a.length);

        // swap first (biggest) element with current-last, reduce heap size and recover
        for (int i = a.length; i > 0; i--) {
            swap(a, 0, i - 1);
            heapify(a, i - 1, 0);
        }
        return a;
    }


    private void buildHeap(int[] A, int n) {

        for (int i = n / 2; i >= 0; i--)
            heapify(A, n, i);
    }


    private void heapify(int[] a, int n, int i) {
        int l = i * 2 + 1;
        int r = i * 2 + 2;

        int m = i;
        if (l < n && a[l] > a[m])
            m = l;
        if (r < n && a[r] > a[m])
            m = r;

        if (m != i) {
            swap(a, i, m);
            heapify(a, n, m);
        }
    }

    // swap two elements of the array
    private void swap(int[] a, int i, int j) {
        int t = a[i];
        a[i] = a[j];
        a[j] = t;
    }

}
