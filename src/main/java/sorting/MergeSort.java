package sorting;

public class MergeSort implements Sorter {

    public int [] sort(int[] a) {

        sort(a, 0, a.length - 1);
        return a;
    }


    private void sort(int[] a, int l, int r) {
        if (l < r) {
            int m = (l + r) / 2;
            sort(a, l, m);
            sort(a, m + 1, r);
            merge(a, l, r, m);
        }
    }


    private void merge(int[] a, int l, int r, int m) {

        int[] left = new int[m - l + 1];
        int[] right = new int[r - m];

        int pl = 0;
        int pr = 0;

        for (int i = l; i <= m; i++)
            left[pl++] = a[i];

        for (int i = m + 1; i <= r; i++)
            right[pr++] = a[i];

        pl = 0;
        pr = 0;

        for (int i = l; i <= r; i++) {

            if (pl < left.length) {
                if (pr < right.length) {
                    if (left[pl] <= right[pr])
                        a[i] = left[pl++];
                    else
                        a[i] = right[pr++];
                } else
                    a[i] = left[pl++];
            } else
                a[i] = right[pr++];
        }
    }

}
