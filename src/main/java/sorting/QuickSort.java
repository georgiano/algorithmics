package sorting;

public class QuickSort implements Sorter{

    // entry point
    public int[] sort(int[] a) {

        return quickSort(a, 0, a.length - 1);
    }


    // recursive quick quickSort algorithm
    private int[] quickSort(int[] a, int l, int r) {

        if (l < r) {
            int q = partition_middle(a, l, r);
            quickSort(a, l, q - 1);
            quickSort(a, q , r); // can be  q + 1 for partition_end partitioner
        }
        return a;
    }


    // partition sub-array A[l..r] and return medien p such that
    // A[l..p-1] <= A[p] && A[p+1..r] > A[p]
    private int partition_end(int[] a, int l, int r) {

        int x = a[r];
        int p = l - 1;

        for (int i = l; i < r; i++ )
            if (a[i] <= x) {
                p++;
                swap(a, p, i);
            }

        p++;
        swap(a, p, r);
        return p;
    }



    private int partition_middle(int[] a, int l, int r) {

        int i = l;
        int j = r;

        int x = a[(l + r) / 2];

        while (i <= j) {
            while (a[i] < x) i++;
            while (a[j] > x) j--;
            if (i <= j) {
                swap(a, i, j);
                i++;
                j--;
            }
        }
        return i;
    }


    // swap two elements of the array
    private void swap(int[] a, int i, int j) {

        int t = a[i];
        a[i] = a[j];
        a[j] = t;
    }
}
