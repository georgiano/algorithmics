import org.junit.Test;

import static org.junit.Assert.*;

public class KMPTest {


    private void test(String s, String p) {

        int i = s.indexOf(p);
        int k = KMP.search(s, p);

        assertEquals("\nfailed searching '" + p + "' inside '" + s + "'", i, k);
    }

    @Test
    public void doTests() {

        test("alfa", "lf");
        test("alfa romeo", "x");
        test("alfa romeo", "alfa");
        test("alfa romeo", "alfo");
        test("aaaaaaab", "aab");
        test("aaaaaaaba", "aba");

    }

    @Test
    public void testInstance() {

        KMP kmp = new KMP("aba");
        assertEquals(kmp.search("aaaaaaa"), -1);
        assertEquals(kmp.search("aaaaaaab"), -1);
        assertEquals(kmp.search("aabaaaaaab"), 1);
        assertEquals(kmp.search("aaaaaaba"), 5);

    }

}