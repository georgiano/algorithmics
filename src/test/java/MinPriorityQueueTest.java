import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;

public class MinPriorityQueueTest {

    private int random(int max) {
        return (int)(Math.random() * max);
    }

    private void printArray(int[] a) {
        for (int i = 0; i < a.length; i++)
            System.out.print(a[i] + ", ");
        System.out.println();
    }

    private void runRandomTest() {
        int n = random(33);
        int[] extracted = new int[n];
        int[] sorted = new int[n];

        for (int i = 0; i < n; i++) {
            extracted[i] = random(100);
            sorted[i] = extracted[i];
        }
        Arrays.sort(sorted);

        printArray(extracted);

        MinPriorityQueue queue = new MinPriorityQueue();
        for (int i = 0; i < n; i++)
            queue.add(extracted[i]);

        for (int i = 0; i < n; i++)
            extracted[i] = queue.extract();

        printArray(extracted);
        printArray(sorted);


        assertArrayEquals(sorted, extracted);
    }

    @Test
    public void randomTests() {
        for (int i = 0; i < 20; i++)
            runRandomTest();
    }

    @Test
    public void test() {
        int[] data = new int[]{78, 1, 51, 76, 38, 37};
        MinPriorityQueue queue = new MinPriorityQueue();
        for (int i = 0; i < data.length; i++)
            queue.add(data[i]);

        while (queue.size() > 0)
            System.out.print(queue.extract() + ", ");
        System.out.println();
    }

}