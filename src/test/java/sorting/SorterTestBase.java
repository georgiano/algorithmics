package sorting;

import org.junit.Test;

import java.util.Arrays;
import java.util.Random;

import static org.junit.Assert.assertArrayEquals;

public class SorterTestBase {

    Sorter sorter;

    private void printArray(int[] A) {
        System.out.print("[");
        for (int i = 0; i < A.length; i++)
            System.out.print((i > 0 ? ", " : "") + A[i]);
        System.out.println("]");
    }

    private void test(int[] A) {

        printArray(A);

        // make sorted copy of source array
        int[] B = new int[A.length];
        for (int i = 0; i < B.length; i++)
            B[i] = A[i];
        Arrays.sort(B);

        //
        sorter.sort(A);
        printArray(A);

        System.out.println();

        assertArrayEquals(A, B);
    }

    @Test
    public void testEmpty() throws Exception {
        test(new int[]{});
    }

    @Test
    public void testSortedAsc() throws Exception {
        test(new int[]{1, 2, 3, 4, 5, 6, 7});
    }

    @Test
    public void testSortedDesc() throws Exception {
        test(new int[]{7, 6, 5, 4, 3, 2, 1});
    }

    @Test
    public void testBasic() throws Exception {

        test(new int[]{7, 1, 5, 8});
        test(new int[]{1, 4, 3, 2});
    }

    @Test
    public void testRandoms() throws Exception {
        for (int i = 0; i < 100; i++)
            testRandom();
    }



    private void testRandom() throws Exception {

        int n = (int) Math.round(Math.random() * 1000);
        int[] A = new int[n];
        for (int i = 0; i < n; i++)
            A[i] = (int) Math.round(Math.random() * 1000);
        test(A);
    }

}